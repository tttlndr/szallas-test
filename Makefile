dc=docker compose

help: ## Show this help
	@echo "Targets:"
	@fgrep -h "##" $(MAKEFILE_LIST) | grep ":" | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/\(.*\):.*##[ \t]*/    \1 ## /' | sort | column -t -s '##'
	@echo

restart: stop start  ## Restart service and DB

start: ## Start service and DB
	$(dc) -f docker-compose.yaml up -d szallas-db szallas-web

stop: ## Stop service and DB
	$(dc) down szallas-db szallas-web

websh:  ## Entry to web container
	$(dc) exec szallas-web bash

dbsh:  ## Entry to DB container
	$(dc) exec szallas-db bash
