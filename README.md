# szallas.hu - teszt feladat

## Install + Company Import

NOTE: a kovetkezo commandok a Makefile-t hasznaljak az egyszeruseg kedveert. A reszleteket ott megtalalod.

Keszits .env filet
```
cp .env.example .env
```

Inditsd el a containert es lepj be
```
make start && make websh
```

Telepitsd az alkalmazas es generalj app key-t
```
composer install && php artisan key:generate --ansi && php artisan migrate
```

Generald seedeld az alapveto resourceokat

```
php artisan db:seed
```

NOTE: az API tesztekhez az auth token a migracio soran kikerul a kimenetre.
Ha elvesztetted akkor a local log elejen talalod ;)

Inditsd ujra az alkalmazast
```
make restart
```

Ellenorizd hogy elindul-e:

http://localhost:8030

Ha fut az alkalmazas akkor a standard Laravel home page kell megjelenjen.

## Company Import

Az importot a DB seed megcsinalja de ha futtatni kell ujra akkor a kontenerben a

```
php artisan app:import-companies
```

command ujra importalja barmikor.
NOTE: a duplikaciokrol debug log keszul.

# Feladatok

## "A regisztráció idejét ne lehessen módosítani (ezt db szinten oldjuk meg, hogy más kódrészlet se tudjon módosítani)"

A `companies.created_at` mezo update tiltasat mezo szintu jogosultsag megvonassal szeretnem megoldani.
Migracio keszult ra a `database/migrations/2024_04_17_150242_alter_companies_created_at.php` fileban.
CLI-ben lefut de hatastalan jelenleg. Ebben a kontenerben a base image init scriptjet kellene okositani a mezo szintu
jogosultsagokkal, de mar nem volt idom jatszani vele.
Ez alapjan indulnek el https://dev.mysql.com/doc/mysql-security-excerpt/8.3/en/grant-tables.html

## Lekerdezesek

### Activity

Hat ez eleg ronda lett, de elvileg azt hozza amit kell.

```mysql
SELECT CONCAT(
           'SELECT `companies`.companyId',
           GROUP_CONCAT(', `t_', REPLACE(activity, '`', '``'), '`.companyName AS `', REPLACE(activity, '`', '``'), '`'
                        SEPARATOR ''),
           ' FROM `companies` ',
           GROUP_CONCAT('LEFT JOIN `companies` AS `t_', REPLACE(activity, '`', '``'), '`
        ON `companies`.companyId = `t_', REPLACE(activity, '`', '``'), '`.companyId
       AND `t_', REPLACE(activity, '`', '``'), '`.activity = ', QUOTE(activity) SEPARATOR ''),
           ' GROUP BY `companies`.companyId'
       )
INTO @qry
FROM (SELECT DISTINCT activity FROM `companies`) t;

PREPARE stmt FROM @qry;
EXECUTE stmt;
```

### "mely cégek alakultak meg"

Ezt nem sikerult munkara birnom :(

A kiindulasi alap amibol az istennek se tudtam 1000-nel tobb iteraciot kicsikarni,
a doksiban max_sp_recursion_depth-et emlitik...

```mysql
WITH RECURSIVE d AS
                   (SELECT '2001-01-01' AS DATE
                    UNION ALL
                    SELECT DATE + INTERVAL 1 DAY
                    FROM d
                    WHERE DATE < curdate())
SELECT *
FROM d;
```

Itt a neveket group_concat()-tal kell majd osszefuzni.

NOTE: a grcontcat max len-t lehet configolni kell ha kilogna a defaultbol!!!

