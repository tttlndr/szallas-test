<?php

namespace App\Http\Controllers;

use App\Http\Requests\FindCompaniesRequest;
use App\Http\Requests\StoreCompanyRequest;
use App\Http\Requests\UpdateCompanyRequest;
use App\Models\Company;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Response;

class CompanyController extends Controller
{

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCompanyRequest $request): JsonResponse
    {
        $company = Company::create($request->validated());
        $company->refresh();

        return response()->json($company, Response::HTTP_CREATED);
    }

    /**
     * Display a listing of the resource.
     */
    public function find(FindCompaniesRequest $request)
    {
        $uniqIds = $this->getUniqCompanyIds($request);
        $companies = Company::whereIn('companyId', $uniqIds)
            ->get();

        return response()->json($companies, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCompanyRequest $request, Company $company)
    {
        $incoming = $request->validated();
        unset($incoming['password']);

        $company->update($incoming);
        $company->refresh();

        return response()->json($company, Response::HTTP_OK);

    }


    private function getUniqCompanyIds(FindCompaniesRequest $request): Collection
    {
        $inputIds = explode(',', $request->validated('ids'));

        $uniqIds = collect($inputIds)->map(function ($id) {
            return abs((int)$id);
        })->unique();

        return $uniqIds;
    }
}
