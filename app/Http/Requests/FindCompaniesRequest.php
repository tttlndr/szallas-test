<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FindCompaniesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true; //TODO may need to check something [andor]
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        //TODO full validation [andor]

        return [
            'ids' => 'required|string',
        ];
    }
}
