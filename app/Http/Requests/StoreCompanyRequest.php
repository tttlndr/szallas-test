<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true; //TODO may need to check something [andor]
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        //TODO full validation [andor]

        return [
            'companyName' => 'required',
            'companyRegistrationNumber' => 'required',
            'companyFoundationDate' => 'required',
            'country' => 'required',
            'zipCode' => 'required',
            'city' => 'required',
            'streetAddress' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'companyOwner' => 'required',
            'employees' => 'required',
            'activity' => 'required',
            'active' => 'required',
            'email' => 'required',
            'password' => 'required',
        ];
    }
}
