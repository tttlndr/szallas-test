<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCompanyRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true; //TODO may need to check something [andor]
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'companyName' => 'sometimes|nullable',
            'companyRegistrationNumber' => 'sometimes|nullable',
            'companyFoundationDate' => 'sometimes|nullable',
            'country' => 'sometimes|nullable',
            'zipCode' => 'sometimes|nullable',
            'city' => 'sometimes|nullable',
            'streetAddress' => 'sometimes|nullable',
            'latitude' => 'sometimes|nullable',
            'longitude' => 'sometimes|nullable',
            'companyOwner' => 'sometimes|nullable',
            'employees' => 'sometimes|nullable',
            'activity' => 'sometimes|nullable',
            'active' => 'sometimes|nullable',
            'email' => 'sometimes|nullable',
            'password' => 'sometimes|nullable|string',
        ];
    }
}
