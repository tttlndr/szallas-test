<?php

namespace App\Console\Commands;

use App\Models\Company;
use Illuminate\Console\Command;
use Illuminate\Database\UniqueConstraintViolationException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use SplFileObject;

class ImportCompanies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:import-companies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Imports companies from file: 'storage/import/testCompanyDB.csv'";

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $file = $this->getFileObject();

        while (!$file->eof()) {
            $line = $file->fgetcsv(';');
            if (empty($line)) {
                continue;
            }
            $this->createCompanyByImportData($line);
        }
    }

    /**
     * @return SplFileObject
     */
    private function getFileObject(): SplFileObject
    {
        $file = new SplFileObject(storage_path(config('company_import.default_source_location')), 'r');
        $file->setFlags(SplFileObject::READ_AHEAD | SplFileObject::SKIP_EMPTY | SplFileObject::DROP_NEW_LINE);
        $file->seek(0);

        return $file;
    }

    /**
     * @param array $line
     * @return void
     */
    private function createCompanyByImportData(array $line): void
    {
        try {
            /** @var Company $company */
            Company::create([
                "companyId" => $line[0],
                "companyName" => $line[1],
                "companyRegistrationNumber" => $line[2],
                "companyFoundationDate" => $line[3],
                "country" => $line[4],
                "zipCode" => $line[5],
                "city" => $line[6],
                "streetAddress" => $line[7],
                "latitude" => (float)$line[8],
                "longitude" => (float)$line[9],
                "companyOwner" => $line[10],
                "employees" => (int)$line[11],
                "activity" => $line[12],
                "active" => strtolower($line[13]) === 'true' ? 1 : 0,
                "email" => $line[14],
                "password" => Hash::make($line[15]),
            ]);
        } catch (UniqueConstraintViolationException $e) {
            Log::debug("Importing company failed - primaryKey already exists", ['company_data' => $line]);
        }
    }
}
