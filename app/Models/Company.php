<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    public const TABLE_NAME = 'companies';

    protected $table = self::TABLE_NAME;

    protected $primaryKey = 'companyId';

    protected $hidden = [
        'password',
    ];

    protected $fillable = [
        "companyId",
        "companyName",
        "companyRegistrationNumber",
        "companyFoundationDate",
        "country",
        "zipCode",
        "city",
        "streetAddress",
        "latitude",
        "longitude",
        "companyOwner",
        "employees",
        "activity",
        "active",
        "email",
        "password",
    ];

    protected $casts = [
        'password' => 'encrypted'
    ];
}
