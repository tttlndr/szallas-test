<?php

namespace Tests;

use App\Models\Company;
use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected ?User $defaultApiUser;

    protected function deleteCompany($companyId): void
    {
        Company::find($companyId)->delete();
        $this->assertDatabaseMissing(Company::TABLE_NAME, ['companyId' => $companyId]);
    }

    protected function getValidCompanyInput(): array
    {
        return [
            'companyName' => fake()->company(),
            'companyRegistrationNumber' => fake()->word(),
            'companyFoundationDate' => fake()->date(),
            'country' => fake()->country(),
            'zipCode' => fake()->postcode(),
            'city' => fake()->city(),
            'streetAddress' => fake()->streetAddress(),
            'latitude' => fake()->latitude(),
            'longitude' => fake()->longitude(),
            'companyOwner' => fake()->name(),
            'employees' => abs(fake()->randomDigitNotNull()),
            'activity' => fake()->word(),
            'active' => fake()->boolean(),
            'email' => fake()->email(),
            'password' => fake()->password(),
        ];
    }

}
