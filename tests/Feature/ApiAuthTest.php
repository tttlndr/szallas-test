<?php

namespace Tests\Feature;

use App\Models\User;
use Laravel\Sanctum\Sanctum;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class ApiAuthTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->defaultApiUser = User::find(1);
    }

    #[Test]
    public function api_user_can_reach_api(): void
    {
        Sanctum::actingAs($this->defaultApiUser);
        $response = $this->get('/api/user');

        $response->assertOk();
    }

    #[Test]
    public function user_without_api_token_cannot_reach_api(): void
    {
        $response = $this->get('/api/user');

        $response->assertRedirect('/');
    }

}
