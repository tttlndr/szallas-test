<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Laravel\Sanctum\Sanctum;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class UpdateCompanyTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->defaultApiUser = User::find(1);
        Sanctum::actingAs($this->defaultApiUser);
    }

    #[Test]
    public function no_id_provided_405(): void
    {
        $response = $this->putJson('/api/companies/', ['irrelevant']);
        $response->assertMethodNotAllowed();
    }

    #[Test]
    public function invalid_data_provided_422(): void
    {
        $response = $this->patchJson('/api/companies/1', ['irrelevant']);

        $response->assertUnprocessable();
        $responseBody = $response->json();

        $this->assertNotEmpty($responseBody['message']);
        $this->assertNotEmpty($responseBody['errors']);
    }

    #[Test]
    public function valid_data_returns_updated_company(): void
    {
        $originalCompany = Company::find(1);

        $updateData = $this->getValidCompanyInput();
        unset($updateData['password']);
        unset($updateData['companyName']);

        $response = $this->patchJson('/api/companies/1', $updateData);

        $response->assertOk();

        $controlData = $updateData;
        $controlData['companyName'] = $originalCompany['companyName'];

        $this->assertDatabaseHas(Company::TABLE_NAME, $controlData);
    }

    public function createdAt_cannot_be_updated(): void
    {
        DB::statement("UPDATE `companies` SET created_at = '2022-11-11 22:11:11' WHERE companyId=1");
        $this->assertTrue(true);
    }


}
