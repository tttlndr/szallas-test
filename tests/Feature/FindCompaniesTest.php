<?php

namespace Tests\Feature;

use App\Models\User;
use Laravel\Sanctum\Sanctum;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class FindCompaniesTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->defaultApiUser = User::find(1);
        Sanctum::actingAs($this->defaultApiUser);
    }

    #[Test]
    public function invalid_data_returns_422(): void
    {
        $response = $this->getJson('/api/companies', []);

        $response->assertUnprocessable();

        $responseBody = $response->json();
        $this->assertNotEmpty($responseBody['message']);
        $this->assertNotEmpty($responseBody['errors']);
    }

    #[Test]
    public function valid_data_returns_unique_companies(): void
    {
        $response = $this->getJson('/api/companies?ids=3,0,adfsdf,1,-2,2,2,3,1,1');

        $response->assertOk();

        $responseBody = $response->json();
        $this->assertCount(3, $responseBody);
    }


}
