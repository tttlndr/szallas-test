<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class CreateCompanyTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->defaultApiUser = User::find(1);
        Sanctum::actingAs($this->defaultApiUser);
    }

    #[Test]
    public function invalid_data_returns_422(): void
    {
        $response = $this->postJson('/api/companies', []);

        $response->assertUnprocessable();

        $responseBody = $response->json();
        $this->assertNotEmpty($responseBody['message']);
        $this->assertNotEmpty($responseBody['errors']);
    }

    #[Test]
    public function valid_data_company_saved(): void
    {
        $newCompanyData = $this->getValidCompanyInput();
        $response = $this->postJson('/api/companies', $newCompanyData);
        $responseData = $response->json();

        $response->assertCreated();

        unset($newCompanyData['password']);
        $this->assertDatabaseHas(Company::TABLE_NAME, $newCompanyData);

        $this->deleteCompany($responseData['companyId']);
    }

    #[Test]
    public function password_saved_as_hash(): void
    {
        $newCompanyData = $this->getValidCompanyInput();
        $originalPassword = $newCompanyData['password'];

        $response = $this->postJson('/api/companies', $newCompanyData);
        $responseData = $response->json();

        $response->assertCreated();

        unset($newCompanyData['password']);
        $this->assertDatabaseHas(Company::TABLE_NAME, $newCompanyData);
        $this->assertDatabaseMissing(Company::TABLE_NAME, ['password' => $originalPassword]);

        $this->deleteCompany($responseData['companyId']);
    }

}
