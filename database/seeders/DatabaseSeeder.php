<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Database\UniqueConstraintViolationException;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{

    public function run(): void
    {
        $this->createDefaultUser();
        Artisan::call('app:import-companies');
    }

    private function createDefaultUser(): void
    {
        try {
            $user = User::factory()->create([
                'id' => 1,
                'name' => 'apiuser',
                'email' => 'apiuser@example.com',
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => Str::random(10),
            ]);
            $this->issueApiTokenForUser($user);

        } catch (UniqueConstraintViolationException $e) {
            // do not break on existing default api user
        }
    }

    private function issueApiTokenForUser(Model|Collection|User $user): void
    {
        $token = $user->createToken('default-api-token');

        $message = "**** API TOKEN for user (id:1): " . $token->plainTextToken;
        echo PHP_EOL . $message . PHP_EOL;
        Log::debug($message, [
            'user' => $user->toArray(),
            'token' => $token->plainTextToken
        ]);
    }
}
