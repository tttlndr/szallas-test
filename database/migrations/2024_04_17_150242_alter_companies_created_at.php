<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration {
    protected $connection = 'mysql_root';

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement(sprintf("REVOKE UPDATE(`created_at`) ON `companies` FROM '%s'", env('DB_USERNAME')));
        DB::statement("flush privileges");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
