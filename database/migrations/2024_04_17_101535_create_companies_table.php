<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigInteger('companyId', true, true);

            $table->string('companyName', 255);
            $table->string('companyRegistrationNumber', 11);
            $table->date('companyFoundationDate');
            $table->string('country', 128);
            $table->string('zipCode', 32);
            $table->string('city', 64);
            $table->string('streetAddress', 128);
            $table->decimal('latitude', 10, 8); // latitudes range from -90 to +90 (degrees)
            $table->decimal('longitude', 11, 8); // longitudes range from -180 to +180 (degrees)
            $table->string('companyOwner', 128);
            $table->integer('employees', false, true);
            $table->string('activity', 128);
            $table->boolean('active');
            $table->string('email', 255); // not unique: same email may belong to several companies
            $table->text('password');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('companies');
    }
};
