<?php

use App\Http\Controllers\CompanyController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
*/
Route::middleware('auth:sanctum')->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::post('/companies', [CompanyController::class, 'store'])->name('registerCompany');
    Route::get('/companies', [CompanyController::class, 'find'])->name('findCompanies');
    Route::patch('/companies/{company}', [CompanyController::class, 'update'])->name('updateCompany');
});
